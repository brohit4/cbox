<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>

	<div class="gamezy-footer">
		<section class="win-rewards">
            <div class="container-fluid reward-details">
              <div class="col-md-12">
                <div class="col-md-12">
                  <h4 class="rewards-title"><b>Win Rewards</b> from the brands you love</h4>
                  <div class="rewards">
                    <div class="brand-logos"><img src="https://www.gamezy.com/images/rewards-1.png" alt="gaana"></div>
                    <div class="brand-logos"><img src="https://www.gamezy.com/images/rewards-2.png" alt="Myntra"></div>
                    <div class="brand-logos"><img src="https://www.gamezy.com/images/rewards-3.png" alt="ClearTrip"></div>
                    <div class="brand-logos"><img src="https://www.gamezy.com/images/rewards-4.png" alt="Box 8"></div>
                    <div class="brand-logos"><img src="https://www.gamezy.com/images/rewards-5.png" alt="Zoomcar"></div>
                    <div class="brand-logos"><img src="https://www.gamezy.com/images/rewards-6.png" alt="ixigo"></div>
                    <div class="brand-logos"><img src="https://www.gamezy.com/images/rewards-7.png" alt="Faasos"></div>
                    <div class="brand-logos"><img src="https://www.gamezy.com/images/rewards-8.png" alt="1 img"></div>
                  </div>
                </div>

              </div>
            </div>
        </section>
		<footer>
            <div class="container-fluid spread-love">
              <div class="col-md-12">
                <div class="col-md-6 spread-love-desc">
                  <h4 class="spread-love-title"> <b>Spread</b> the <b>Love</b></h4>
                  <p>Tell your friends about Gamezy</p>
                </div><div class="col-md-6 spread-love-social">
                  <div class="social">
                    <a href="https://www.facebook.com/Gamezyapp/"><img src="https://www.gamezy.com/images/facebook.png" alt="Facebook App"></a>
                    <a href="https://twitter.com/Gamezyapp/">
                      <img src="https://www.gamezy.com/images/twitter.png" alt="Twitter App"></a>
                    <a href="https://www.instagram.com/gamezyapp_india/">
                      <img src="https://www.gamezy.com/images/insta.png" alt="Instagram App"></a>
                  </div>
                </div>
              </div>
            </div>

            <div class="container-fluid bottom-footer">
              <div class="col-md-12 col-sm-12 bottom-logo">
                <img src="https://www.gamezy.com/images/bottom.svg" alt="Gamezy Logo">
                <h4><b>Gamezy</b> is the latest game from the house of GamesKraft-<br>
                  <b>India's fastest-growing real money game creators</b></h4>
                </div>
                <div class="col-md-12">
                 <div class="col-md-3 footer-info">
                   <div class="sep">
                     <h3>5,00,000</h3>
                     <p>active users across India</p>
                   </div>
                 </div>
                 <div class="col-md-3 footer-info">
                   <div class="sep">
                     <h3><em class="indian-rupee">₹</em> 10 Crore+</h3>
                     <p>Cash won every day</p>
                   </div>
                 </div>
                 <div class="col-md-3 footer-info">
                   <div class="sep">
                     <h3>1 Million</h3>
                     <p>cash games every day</p>
                   </div>
                 </div>
                 <div class="col-md-3 footer-info">
                   <div class="sep">
                     <h3>Member</h3>
                     <p>of all india Gaming Federation</p>
                   </div>
                 </div>
               </div>
			 </div>
			 <div class="container-fluid at-last">
             <div class="b-f">
               <div class="col-md-8 b-left">
                <ul>
                  <li  class="b-li"><a class="b-li-a" href="mailto:support@gamezy.com">support@gamezy.com</a></li>
                  <!-- <li  class="b-li"><a class="b-li-a" href="">About Us</a></li>
                  <li  class="b-li"><a class="b-li-a" href="">Contact Us</a></li> -->
                  <li  class="b-li"><a class="b-li-a" href="terms">Terms &amp; Conditions</a></li>
                  <li  class="b-li"><a class="b-li-a" href="privacy-policy">Privacy Policy</a></li>
                </ul>
              </div><div class="col-md-4 b-right">
                <p class="text-right"><i class="fa fa-copyright" aria-hidden="true"></i> Gamezy 2019</p>
              </div>
            </div>
          </div>
           </footer>
	</div>
</div><!-- #page -->
<?php wp_footer(); ?>
<link href="https://fonts.googleapis.com/css?family=Barlow:400,600" rel="stylesheet">
</body>
</html>
