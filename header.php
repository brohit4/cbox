<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<?php include( locate_template( 'template-parts/variables.php', false, false ) ); ?>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="shortcut icon" href="<?php  echo get_template_directory_uri() ?>/assets/images/logo.ico" />
<?php wp_head(); ?>
<link rel="preload" href="<?php echo THEME_CSS_PATH ?>/style.css" as="style">
</head>

<body <?php body_class(); ?>>
<script type="text/javascript">
	function loadStyleSheet(src) {
		src = src || '<?php echo THEME_CSS_PATH ?>/style.css';
		if (document.createStyleSheet){
			document.createStyleSheet(src);
		}
		else {
			var link = document.createElement('link');
			
			link.rel = 'stylesheet';
			link.type = 'text/css';
			link.media = 'screen';
			link.href = src;
			document.head.appendChild(link);
			
			//$("head").append($("<link rel='stylesheet' href='"+src+"' type='text/css' media='screen' />"));
		}
	};
	
</script>
<div id="page" class="site">
	<!-- <a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'twentyseventeen' ); ?></a> -->

	

	<?php

	/*
	 * If a regular post or page, and not the front page, show the featured image.
	 * Using get_queried_object_id() here since the $post global may not be set before a call to the_post().
	 */
	if ( ( is_single() || ( is_page() && ! twentyseventeen_is_frontpage() ) ) && has_post_thumbnail( get_queried_object_id() ) ) :
		echo '<div class="single-featured-image-header">';
		echo get_the_post_thumbnail( get_queried_object_id(), 'twentyseventeen-featured-image' );
		echo '</div><!-- .single-featured-image-header -->';
	endif;
	?>

	<div class="site-content-contain">
		<div id="content" class="site-content">
