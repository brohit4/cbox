loadStyleSheet();
$(function() {
    var hideScroll = function() {
        $('body, html').css({
            overflow: 'hidden',
            position: 'relative',
            height: '100%'
        });
        
    };
    var showScroll = function() {
        $('body, html').css({
            overflow: 'auto',
            position: 'static',
            height: 'initial'
        });
    };
    function gkModal(sel) {
        $(sel).show();
        hideScroll();

        var closeModal = function(event) {
            var popup  = $(sel);
            popup.hide();
           // popup.find('.rc-modal-box').html('');
            showScroll();
            $(sel).off('click', '.modal-close-icon', closeModal); 
            $(document).off('keyup', escCloseModal);
        };
        var escCloseModal = function(e) {
            if (e.key === "Escape" || e.key === "Esc" || e.keyCode == 27) { // escape key maps to keycode `27`
             closeModal();
           }
       };
       $(sel).on('click', '.modal-close-icon', closeModal); 
       $(document).on('keyup', escCloseModal);
       return closeModal;
    }


    $('.recording-video').on('click', function() {
        gkModal('.video-popup');
    })


   
    const submitLink = document.querySelector('.btn-submit');
    const stopButton = document.querySelector('.stop-recording-btn');
    const startButton = document.querySelector('.start-recording-btn');
   
    var uploadVideo = function(recordedChunks) {
        var formData = new FormData();
        var blob = new Blob(recordedChunks);
        var filetoUpload = new File([blob], 'acetest.webm');
        formData.append('video', filetoUpload);
        formData.append('user_id', 4);

        closeLoadingModal = gkModal('.uploading.rc-modal-wrap');
        $.ajax({
            url: 'http://13.127.127.146/api/commentary/upload_commentary_video',
            type: 'POST',
            data: formData,
            contentType: false,
            processData: false,
            success: function() {
                closeLoadingModal();
                gkModal('.uploaded.rc-modal-wrap')
            },
            error: function() {
                closeLoadingModal();
                gkModal('.failed.rc-modal-wrap')
            }
        })
    };
   
    var handleSuccess = function(stream) {
        var player = document.querySelector('.video-preview');
        var originalPlayer = document.querySelector('.video-el');
        const options = {mimeType: 'video/webm'};
        const recordedChunks = [];
        const mediaRecorder = new MediaRecorder(stream, options);


        var stopStreaming = function() {
            mediaRecorder.stop();
            stopButton.style.display = 'none';
            player.autoplay = false;
            
            player.srcObject = undefined;
            stream.getTracks() // get all tracks from the MediaStream
            .forEach( function(track) {
    
             track.stop();
            }); // stop each of them
    
            originalPlayer.muted = false;
            originalPlayer.currentTime = 0;
            originalPlayer.controls = true;
            player.style.display = 'none';
            //originalPlayer.stop();
            originalPlayer.removeEventListener('ended', playerEnd);
            submitLink.addEventListener('click', function() {
                uploadVideo(recordedChunks);
            })
            $('.btn-preview').on('click', function() {
                $('.sample-video').hide();
                $('.video-final-preview').attr('src', URL.createObjectURL(new Blob(recordedChunks))).show();
            });
        };

        var playerEnd = function() {
            stopStreaming();
            $('.preview-section').show();
            $('.stop-recording-btn').hide();
        };

        player.style.display = 'block';
        startButton.style.display = 'none';
        stopButton.style.display = 'initial';
       

        player.autoplay = true;
        player.srcObject = stream;
        player.muted = true;

        originalPlayer.muted = true;
        originalPlayer.currentTime = 0;
        originalPlayer.controls = false;

       
        originalPlayer.addEventListener('ended', playerEnd);
        originalPlayer.play();
  
     
      stopButton.addEventListener('click', function() {
       
       // download.style.display = 'initial';
        startButton.style.display = 'initial';
        
        stopStreaming();
      })
  
      mediaRecorder.onerror = function(error) { console.error(error); }; mediaRecorder.onwarning = function(warning) { console.warn(warning); }; 
  
      //mediaRecorder.addEventListener('dataavailable', function(e) {
      mediaRecorder.ondataavailable =  function(e) {
        if (e.data.size > 0) {
          recordedChunks.push(e.data);
        }
  
        // if(shouldStop === true && stopped === false) {
        //   mediaRecorder.stop();
        //   stopped = true;
        // }
      };
  
      mediaRecorder.addEventListener('stop', function(e) {
       // downloadLink.href = URL.createObjectURL(new Blob(recordedChunks));
        //downloadLink.download = 'acetest.webm';
        //uploadVideo(recordedChunks);
      });
  
      mediaRecorder.start();
    };
    startButton.addEventListener('click', function() {
      $('.btn-submit').off();
      $('.btn-preview').off();
      navigator.mediaDevices.getUserMedia({ audio: true, video: true })
        .then(handleSuccess);
    });

    


    
});