<?php
/*
Template Name: Video
*/

get_header(); ?>

<div id="primary" class="content-area  video-page-content">
    <?php get_template_part( 'template-parts/header/content', 'header' ); ?>
    <?php
        $pageid = get_queried_object_id();
        $videolink = get_post_meta( $pageid, 'video-url', true );


    ?>
    <div class="video-content">
        <video src="<?php echo THEME_IMG_PATH ?>/<?php echo  $videolink ?>" class="video-el sample-video" controls></video>
        <video  class="video-el video-final-preview" controls></video>
        <video class="video-preview"></video>
        <button class="start-recording-btn recording-btn lg"><?php get_template_part( 'template-parts/page/start-recording', 'btn' ); ?> </button>
        <button class="stop-recording-btn recording-btn lg"><?php get_template_part( 'template-parts/page/stop-recording', 'btn' ); ?> </button>
        <!-- <a id="download" style="display: none;">Download</a> -->
        <div class="preview-section">
            <p class="preview-desc">Yay. Awesome!<br/>
                <span class="bold">Your video has been recorded</span>
            </p>
            <div class="clearfix">
                <button class="btn btn-preview">PREVIEW</button>
                <button class="btn btn-submit primary">SUBMIT</button>
                <span class="re-record-desc">Not happy with the recording? <span class="re-record-link">Rerecord</span></span>
            </div>
        </div>
    </div>
    <div class="upload-status uploading rc-modal-wrap" style="display: none;">
	
        <div class="rc-modal-box upload-status-c">
        <svg class="svg-icon modal-close-icon">
            <?php include(get_stylesheet_directory().'/assets/svg/path.svg'); ?>
            </svg>
        <div class="upload-title">Your recording is being
    uploaded</div><div>
    <svg class="svg-icon status-svg">
            <?php include(get_stylesheet_directory().'/assets/svg/oval.svg'); ?>
            </svg></div>
        <p class="upload-desc">Once uploaded this may take sometime to
                reflect on our platform. You can access the video in your pofile</p>
        </div>
    </div>
    <div class="upload-status uploaded rc-modal-wrap" style="display: none;">
        
        <div class="rc-modal-box upload-status-c">
        <svg class="svg-icon modal-close-icon">
            <?php include(get_stylesheet_directory().'/assets/svg/path.svg'); ?>
            </svg>
        <div class="upload-title">Your video has been submitted</div><div>
    <svg class="svg-icon status-svg">
            <?php include(get_stylesheet_directory().'/assets/svg/tick.svg'); ?>
            </svg></div>
        <p class="upload-desc">Once uploaded this may take sometime to
                reflect on our platform. You can access the video in your pofile</p>
        </div>
    </div>
    <div class="upload-status failed rc-modal-wrap" style="display: none;">
        
        <div class="rc-modal-box upload-status-c">
        <svg class="svg-icon modal-close-icon">
            <?php include(get_stylesheet_directory().'/assets/svg/path.svg'); ?>
            </svg>
        <div class="upload-title">Your video could not be uploaded</div><div>
    <svg class="svg-icon status-svg">
            <?php include(get_stylesheet_directory().'/assets/svg/tick.svg'); ?>
            </svg></div>
        <p class="upload-desc">Please retry after sometime</p>
        </div>
    </div>
</div>
<script type="text/javascript" defer src="<?php echo THEME_JS_PATH ?>/video-page.js"></script>
<?php get_footer();