<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<div class="desktop-wrap">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
</div>
	</header><!-- .entry-header -->
	<div class="entry-content">
		<div class="desktop-wrap">
		<?php
			the_content();

			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'twentyseventeen' ),
				'after'  => '</div>',
			) );
		?>
		</div>
	</div><!-- .entry-content -->
</article><!-- #post-## -->
