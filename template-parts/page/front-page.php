<?php
/**
 * The front page template file
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 * 
 */

get_header(); ?>
<!-- <video id="player" controls src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/acetest.webm"></video>
<script>


//   navigator.mediaDevices.getUserMedia({ audio: true, video: true })
//       .then(handleSuccess)

</script>
<a id="download">Download</a>
<button id="stop">Stop</button>
<button id="start">Start</button>
<script>
  let shouldStop = false;
  let stopped = false;
  const downloadLink = document.getElementById('download');
  const stopButton = document.getElementById('stop');
  const startButton = document.getElementById('start');
 

  var handleSuccess = function(stream) {
	// var player = document.getElementById('player');


    //  player.srcObject = stream;

    const options = {mimeType: 'video/webm'};
    const recordedChunks = [];
    const mediaRecorder = new MediaRecorder(stream, options);
    stopButton.addEventListener('click', function() {
      mediaRecorder.stop();
    })

    mediaRecorder.onerror = function(error) { console.error(error); }; mediaRecorder.onwarning = function(warning) { console.warn(warning); }; 

    //mediaRecorder.addEventListener('dataavailable', function(e) {
    mediaRecorder.ondataavailable =  function(e) {
      if (e.data.size > 0) {
        recordedChunks.push(e.data);
      }

      // if(shouldStop === true && stopped === false) {
      //   mediaRecorder.stop();
      //   stopped = true;
      // }
    };

    mediaRecorder.addEventListener('stop', function(e) {
      downloadLink.href = URL.createObjectURL(new Blob(recordedChunks));
      downloadLink.download = 'acetest.webm';
    });

    mediaRecorder.start();
  };
  startButton.addEventListener('click', function() {
    navigator.mediaDevices.getUserMedia({ audio: true, video: true })
      .then(handleSuccess);
  })
  

</script> -->

<div id="primary" class="content-area  home-page-content">
	<main id="main" class="site-main" role="main">
		<div class="desktop-wrap">

		<?php // Show the selected frontpage content.
		// if ( have_posts() ) :
		// 	while ( have_posts() ) : the_post();
		// 		get_template_part( 'template-parts/page/content', 'front-page' );
		// 	endwhile;
		// else :
		// 	get_template_part( 'template-parts/post/content', 'none' );
		// endif; ?>
      <?php get_template_part( 'template-parts/header/content', 'header' ); ?>
      <div class="main-content">
		<div class="desktop-main">
			<h1>Think like <label class="money-label">an expert</label><br> Talk like <label class="money-label">a commentator</label></h1>
      
      <p class="contract"><span class="bold">Win Rs 50,000</span> season contract*</p>
      <div class="mobile-only play-video play-video-main">
            <svg class="play-svg"><?php include(get_stylesheet_directory().'/assets/svg/play-button-1.svg'); ?></svg>
            <span class="watch-video" >Watch Video</span>
        </div>
      <ul class="sign-up-steps">
      <li class="sign-up-step">
        <div class="sign-up-desc"><span class="bold"><span class="step-no">1</span>Select </span> one of our featured videos</div>
      </li><li class="sign-up-step">
      <div class="sign-up-desc"><span class="step-no">2</span><span class="bold">Record</span> your voice</div>
      </li><li class="sign-up-step">
      <div class="sign-up-desc"><span class="step-no">3</span><span class="bold">Submit</span> and you are good to go</div>
      </li>
    </ul>
    
		</div>
		
    <svg class="play-svg desktop-only play-video-main"><?php include(get_stylesheet_directory().'/assets/svg/play-button-1.svg'); ?></svg>

    </div>
		</div>
		
	</main><!-- #main -->

  <!-- <section class="sign-up">
    <h2 class="sign-up-title">How to <span class="bold">sign up?</span></h2>
    <ul class="sing-up-steps">
      <li class="sign-up-step">
        <svg class="sign-up-svg"><?php include_once(get_stylesheet_directory().'/assets/svg/mobile-app-copy-2.svg'); ?></svg><div class="sign-up-desc"><span class="bold">Select one</span> of our featured videos for commentary</div>
      </li><li class="sign-up-step">
        <svg class="sign-up-svg"><?php include_once(get_stylesheet_directory().'/assets/svg/video-camera-copy-3.svg'); ?></svg><div class="sign-up-desc"><span class="bold">Practice your</span> lines and style of commentary</div>
      </li><li class="sign-up-step">
        <svg class="sign-up-svg"><?php include_once(get_stylesheet_directory().'/assets/svg/mobile-app-copy-3.svg'); ?></svg><div class="sign-up-desc"><span class="bold">Record</span> with us as many times until satisfied</div>
      </li><li class="sign-up-step">
        <svg class="sign-up-svg"><?php include_once(get_stylesheet_directory().'/assets/svg/video-camera-copy-4.svg'); ?></svg><div class="sign-up-desc"><span class="bold"> Submit</span> and you are good to go</div>
      </li>
    </ul>
  </section> -->
  <section class="start-recording">
      <h2 class="">
        <svg class="recording-svg"><?php include(get_stylesheet_directory().'/assets/svg/group-28-copy.svg'); ?></svg><div class="start-recording-title">
          <span>Select a video and</span>
          <span class="start-commentary-text bold">start your commentary</span>
        </div>
      </h2>
      <ul class="recording-videos">
      <?php
               $args = array(
                    'sort_order' => 'asc',
                    'sort_column' => 'post_title',
                    'hierarchical' => 1,
                    'exclude' => '',
                    'include' => '',
                    'meta_key' => 'video-page',
                    'meta_value' => 'true',
                    'authors' => '',
                    'child_of' => 0,
                    'parent' => -1,
                    'exclude_tree' => '',
                    'number' => '',
                    'offset' => 0,
                    'post_type' => 'page',
                    'post_status' => 'publish'
                ); 
                $pages = get_pages($args); 
            

                

                foreach( $pages as $page ) {		
                    $content = $page->post_content;
                    // if ( ! $content ) // Check for empty page
                    //     continue;
            
                    $content = apply_filters( 'the_content', $content );
            ?>  
                <li class="recording-video" data-href="<?php echo get_page_link( $page->ID ); ?>">
                <div class="recording-visual">
                  <img class="recording-thumb" src="<?php echo THEME_IMG_PATH ?>/bitmap.jpg" 		   srcset="<?php echo THEME_IMG_PATH ?>/bitmap.jpg,
             
                  <?php echo THEME_IMG_PATH ?>/bitmap@2x.jpg 2x"/>
                  <svg class="play-recording-svg"><?php include(get_stylesheet_directory().'/assets/svg/play-button-1.svg'); ?></svg>
                  <span class="recording-ts">00:30</span>
                  <span class="recording-label">VD01</span>
                </div>
                <div class="recording-video-info">      
          <div class="recording-video-title"><?php echo $page->post_title; ?></div>
          <!-- <div class="recording-video-info">
            <label class="recording-video-label">Duration:</label>
            <span class="recording-video-value">30 Sec</span>
            <label class="recording-video-label">Used:</label>
            <span class="recording-video-value">320 people</span>
          </div> -->
          <button class="recording-btn"><?php get_template_part( 'template-parts/page/start-recording', 'btn' ); ?> </button>
                </div>
        </li>
                
            <?php
                }	
            ?>
        
      </ul>
  </section>
  <section class="submitted-videos-section">
      <h2 class="">
        <svg class="recording-svg"><?php include(get_stylesheet_directory().'/assets/svg/group-28-copy.svg'); ?></svg><div class="start-recording-title">
          <span class="bold">Submitted Videos</span>
          
        </div>
      </h2>
      <ul class="submitted-videos">
        <li class="submitted-video">
         <video src="<?php echo THEME_IMG_PATH ?>/acetest.webm"  controls class="submitted-video-thumb"></video>
          <div class="submitted-video-info">
            <div class="submitted-video-author">Posted by John Deo</div>
            <div class="submitted-video-date">on 26th Jan 2019</div>
          </div>
        </li> <li class="submitted-video">
         <video src="<?php echo THEME_IMG_PATH ?>/acetest_2.webm"  controls class="submitted-video-thumb"></video>
          <div class="submitted-video-info">
            <div class="submitted-video-author">Posted by John Deo</div>
            <div class="submitted-video-date">on 26th Jan 2019</div>
          </div>
        </li> <li class="submitted-video">
         <video src="<?php echo THEME_IMG_PATH ?>/sample.mp4"  controls class="submitted-video-thumb"></video>
          <div class="submitted-video-info">
            <div class="submitted-video-author">Posted by John Deo</div>
            <div class="submitted-video-date">on 26th Jan 2019</div>
          </div>
        </li>
      </ul>
    </section>
    	
	<section class=" opinion">
        <div class="desktop-wrap">
          <div class="opinion-wrap">
			<p class="opinion-title">
            Do you have an opinion on everything?<br> <span class="bold">Then come speak with us.</span>
            </p>
			<p class="opinion-desc">
                Do you possess the knowledge of Harsha Bhogle, the insight of Sunil Gavaskar and the humour of Siddhu? If yes, then we have you covered. Join the Gamezy Commentary Box contest and become an expert commentator of the gentleman’s game. Record yourself commenting and analyzing a short clip from any match, send it to us and you could be in the top 100 entries selected for an expert panel of commentators. Make it to the best 5 and we will groom you and give you the platform for voicing your opinion live on Kohli’s big innings or Malinga’s big spell.
            </p>
            <p class="opinion-question">What are you waiting for? Join now and be heard by millions.</p>
    </div>
        </div>
  </section>
    <div class="video-popup rc-modal-wrap" style="display: none;">
	
      <div class="rc-modal-box">
        <svg class="svg-icon modal-close-icon">
          <?php include(get_stylesheet_directory().'/assets/svg/path.svg'); ?>
          </svg>
        <video src="<?php echo THEME_IMG_PATH ?>/sample.mp4" class="video-el" controls></video>
        <video class="video-preview"></video>
        <a href="" class="start-recording-link">
        <button class="start-recording-btn recording-btn lg"><?php get_template_part( 'template-parts/page/start-recording', 'btn' ); ?> </button>
        </a>
        <a id="download" style="display: none;">Download</a>
      </div>
  </div>
  <div class="main-video-popup rc-modal-wrap" style="display: none;">
	
      <div class="rc-modal-box">
        <svg class="svg-icon modal-close-icon">
          <?php include(get_stylesheet_directory().'/assets/svg/path.svg'); ?>
          </svg>
        <video src="<?php echo THEME_IMG_PATH ?>/sample.mp4" class="video-el" controls></video>
       
      </div>
  </div>
  
</div><!-- #primary -->

<script type="text/javascript" defer src="<?php echo THEME_JS_PATH ?>/video-home.js"></script>

<?php get_footer();
